-- Proyecto de la primera práctica de la asignatura Diseño de Sistemas Operativos --

- Alumno:	Jesús María Gómez Moreno
- DNI:		75917069A

Preguntas:
	Makefile
	-¿Qué comando lo puede ejecutar si se llama Makefile2?
	+make -f Makefile2.

	-Si vuelves a utilizar Makefile2, ¿qué ocurre?
	+Me informa de que el fichero ya está actualizado (make: 'bin/programa' is up to date).

	-Vuelve a lanzar el Makefile2, ¿qué ocurre? Asegúrate de que es lo correcto, si no corrige tu Makefile2
	+Puesto que una de las librerías de las que depende el programa ha sido modificada, el programa debe recompilarse, que es lo que ocurre. Podemos comprobar que ocurre lo mismo al modificar libreria.c.